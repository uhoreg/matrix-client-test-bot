/*
Copyright 2021 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import anotherjson from "another-json";

import { TestRunner } from "../runner";
import { makeTxnId, verifySignedJson, JsonObj } from "../util";

/** Test for fallback keys */
export async function test(runner: TestRunner) {
    runner.debug("Logging in");
    const devices = await Promise.all([
        runner.logIn(), runner.logIn(), runner.logIn(),
    ]);

    const accounts = [new global.Olm.Account(), new global.Olm.Account(), new global.Olm.Account()];
    accounts.forEach((account) => { account.create(); });
    const identityKeys = accounts.map(account => JSON.parse(account.identity_keys()));

    await Promise.all(devices.map(async (device, idx) => {
        const account = accounts[idx];
        const deviceKeys: Record<string, any> = {
            algorithms: [
                "m.olm.v1.curve25519-aes-sha2",
                "m.megolm.v1.aes-sha2"
            ],
            device_id: device.deviceId,
            keys: {
                [`ed25519:${device.deviceId}`]: identityKeys[idx].ed25519,
                [`curve25519:${device.deviceId}`]: identityKeys[idx].curve25519,
            },
            user_id: device.userId,
        }
        const signature = account.sign(anotherjson.stringify(deviceKeys));
        deviceKeys.signatures = {
            [device.userId]: {
                [`ed25519:${device.deviceId}`]: signature,
            },
        };
        await device.http.authedRequest(
            undefined, "POST", "/keys/upload", undefined, {
                device_keys: deviceKeys,
            },
        );
    }));

    runner.log("Log in with your client.")
    const targetUserId = await runner.prompt("What is your user ID?");
    const targetDeviceId = await runner.prompt("What is your device ID?");
    await runner.waitForUser("Please close the client.");

    let res: Record<string, any>;

    res = await devices[0].http.authedRequest(
        undefined, "POST", "/keys/query", undefined, {
            device_keys: {
                [targetUserId]: [targetDeviceId],
            }
        }
    );

    if (!res.device_keys
        || !res.device_keys[targetUserId]
        || !res.device_keys[targetUserId][targetDeviceId]) {
        throw new Error("No device key found");
    }
    // FIXME: check signature
    const targetDeviceKeys = {
        ed25519: res.device_keys[targetUserId][targetDeviceId].keys[`ed25519:${targetDeviceId}`],
        curve25519: res.device_keys[targetUserId][targetDeviceId].keys[`curve25519:${targetDeviceId}`],
    };

    // claim all the one-time keys, until we get a fallback key
    runner.debug("Draining one-time keys");
    let prevOtkId: string = "";
    let fallbackKey: string;
    const claimData = {
        one_time_keys: {
            [targetUserId]: {
                [targetDeviceId]: "signed_curve25519",
            }
        }
    }
    for (;;) {
        res = await devices[0].http.authedRequest(
            undefined, "POST", "/keys/claim", undefined, claimData,
        );
        if (!res.one_time_keys[targetUserId] || !res.one_time_keys[targetUserId][targetDeviceId]) {
            throw new Error("No fallback key found");
        }
        const [keyId, key] = [...Object.entries(res.one_time_keys[targetUserId][targetDeviceId])][0] as [string, {fallback?: boolean, key: string}];
        verifySignedJson(key, targetDeviceKeys.ed25519, targetUserId, targetDeviceId);
        if (key.fallback) {
            fallbackKey = key.key;
            break;
        } else if (keyId === prevOtkId) {
            runner.warn("Fallback key detected, but no fallback property given.");
            fallbackKey = key.key;
            break;
        } else {
            prevOtkId = keyId;
        }
    }

    runner.debug("Creating olm and megolm sessions");
    const sessions = [new global.Olm.Session(), new global.Olm.Session(), new global.Olm.Session()];
    sessions.forEach((session, idx) => { session.create_outbound(accounts[idx], targetDeviceKeys.curve25519, fallbackKey); });

    const megolmSessions = [
        new global.Olm.OutboundGroupSession(),
        new global.Olm.OutboundGroupSession(),
        new global.Olm.OutboundGroupSession(),
    ];
    megolmSessions.forEach((session) => { session.create(); });

    devices[0].startSync();

    const roomId = await devices[0].createRoom({encrypted: true, invite: [targetUserId]});
    runner.cleanup(async () => {
        // FIXME: kick targetUserId from the room?
        await devices[0].http.authedRequest(
            undefined, "POST", `/rooms/${roomId}/leave`, undefined, {}
        );
    });

    await devices[0].http.authedRequest(
        undefined, "POST", `/rooms/${roomId}/state/m.room.encryption/`, {
            algorithm: "m.megolm.v1.aes-sha2",
        },
    );

    // send first key
    {
        const plaintext = {
            type: "m.room_key",
            content: {
                algorithm: "m.megolm.v1.aes-sha2",
                room_id: roomId,
                session_id: megolmSessions[0].session_id(),
                session_key: megolmSessions[0].session_key(),
            },
            sender: devices[0].userId,
            recipient: targetUserId,
            recipient_keys: {
                ed25519: targetDeviceKeys.ed25519,
            },
            keys: {
                ed25519: identityKeys[0].ed25519,
            },
        };
        const encryptedKeyEvent = {
            algorithm: "m.olm.v1.curve25519-aes-sha2",
            sender_key: identityKeys[0].curve25519,
            ciphertext: {
                [targetDeviceKeys.curve25519]: sessions[0].encrypt(JSON.stringify(plaintext)),
            },
        };

        const toDeviceTxnId = makeTxnId();
        await devices[0].http.authedRequest(
            undefined, "PUT", `/sendToDevice/m.room.encrypted/${toDeviceTxnId}`,
            undefined, {
                messages: {
                    [targetUserId]: {
                        [targetDeviceId]: encryptedKeyEvent,
                    },
                },
            },
        );

        const roomTxnId = makeTxnId();
        const roomEvent = {
            type: "m.room.message",
            room_id: roomId,
            content: {
                body: "Hello world!",
                msgtype: "m.text",
            },
        }
        await devices[0].http.authedRequest(
            undefined, "PUT", `/rooms/${roomId}/send/m.room.encrypted/${roomTxnId}`,
            undefined, {
                algorithm: "m.megolm.v1.aes-sha2",
                sender_key: identityKeys[0].curve25519,
                ciphertext: megolmSessions[0].encrypt(JSON.stringify(roomEvent)),
                session_id: megolmSessions[0].session_id(),
                device_id: devices[0].deviceId,
            },
        );
    }

    // send second key
    {
        const plaintext = {
            type: "m.room_key",
            content: {
                algorithm: "m.megolm.v1.aes-sha2",
                room_id: roomId,
                session_id: megolmSessions[1].session_id(),
                session_key: megolmSessions[1].session_key(),
            },
            sender: devices[1].userId,
            recipient: targetUserId,
            recipient_keys: {
                ed25519: targetDeviceKeys.ed25519,
            },
            keys: {
                ed25519: identityKeys[1].ed25519,
            },
        };
        const encryptedKeyEvent = {
            algorithm: "m.olm.v1.curve25519-aes-sha2",
            sender_key: identityKeys[1].curve25519,
            ciphertext: {
                [targetDeviceKeys.curve25519]: sessions[1].encrypt(JSON.stringify(plaintext)),
            },
        };

        const toDeviceTxnId = makeTxnId();
        await devices[1].http.authedRequest(
            undefined, "PUT", `/sendToDevice/m.room.encrypted/${toDeviceTxnId}`,
            undefined, {
                messages: {
                    [targetUserId]: {
                        [targetDeviceId]: encryptedKeyEvent,
                    },
                },
            },
        );

        const roomTxnId = makeTxnId();
        const roomEvent = {
            type: "m.room.message",
            room_id: roomId,
            content: {
                body: "Hello again!",
                msgtype: "m.text",
            },
        }
        await devices[1].http.authedRequest(
            undefined, "PUT", `/rooms/${roomId}/send/m.room.encrypted/${roomTxnId}`,
            undefined, {
                algorithm: "m.megolm.v1.aes-sha2",
                sender_key: identityKeys[1].curve25519,
                ciphertext: megolmSessions[1].encrypt(JSON.stringify(roomEvent)),
                session_id: megolmSessions[1].session_id(),
                device_id: devices[1].deviceId,
            },
        );
    }

    runner.log(`Re-open the client and join the room that you were invited to by ${devices[0].userId}.  Upon joining, you should be able to read two messages.`);
    await devices[0].waitForEvent(
        event => event.room_id === roomId && event.type === "m.room.member"
            && event.state_key === targetUserId
            && event.content.membership === "join",
    );

    runner.log("Waiting for 6 minutes...");

    await new Promise((resolve, reject) => {
        setTimeout(resolve, 6*60*1000);
    });

    // send third key
    {
        const plaintext = {
            type: "m.room_key",
            content: {
                algorithm: "m.megolm.v1.aes-sha2",
                room_id: roomId,
                session_id: megolmSessions[2].session_id(),
                session_key: megolmSessions[2].session_key(),
            },
            sender: devices[2].userId,
            recipient: targetUserId,
            recipient_keys: {
                ed25519: targetDeviceKeys.ed25519,
            },
            keys: {
                ed25519: identityKeys[2].ed25519,
            },
        };
        const encryptedKeyEvent = {
            algorithm: "m.olm.v1.curve25519-aes-sha2",
            sender_key: identityKeys[2].curve25519,
            ciphertext: {
                [targetDeviceKeys.curve25519]: sessions[2].encrypt(JSON.stringify(plaintext)),
            },
        };

        const toDeviceTxnId = makeTxnId();
        await devices[2].http.authedRequest(
            undefined, "PUT", `/sendToDevice/m.room.encrypted/${toDeviceTxnId}`,
            undefined, {
                messages: {
                    [targetUserId]: {
                        [targetDeviceId]: encryptedKeyEvent,
                    },
                },
            },
        );

        const roomTxnId = makeTxnId();
        const roomEvent = {
            type: "m.room.message",
            room_id: roomId,
            content: {
                body: "This message should not be decryptable.",
                msgtype: "m.text",
            },
        }
        await devices[1].http.authedRequest(
            undefined, "PUT", `/rooms/${roomId}/send/m.room.encrypted/${roomTxnId}`,
            undefined, {
                algorithm: "m.megolm.v1.aes-sha2",
                sender_key: identityKeys[2].curve25519,
                ciphertext: megolmSessions[2].encrypt(JSON.stringify(roomEvent)),
                session_id: megolmSessions[2].session_id(),
                device_id: devices[2].deviceId,
            },
        );
    }

    // FIXME: in theory, we should be able to detect that the message failed to
    // decrypt because we will get a keyshare request from the device (if the
    // client supports that) and/or an olm unwedging message
    if (await runner.promptYN("There should now be a third message in the room, which you should not be able to decrypt.  Are you able to decrypt it?")) {
        throw new Error("The fallback key was not expired");
    }

    // FIXME: we should get the user to close the client again, and reclaim all
    // the keys and check that they've uploaded a new fallback key.
}
