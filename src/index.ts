/*
Copyright 2021 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import { readFileSync } from "fs";
import request from "request";
import Olm from "@matrix-org/olm";

import { TestRunner } from "./runner";

import { test } from "./tests/fallback";

global.Olm = Olm;

// FIXME: this should take a cmd line argument that indicates which test to run
(async () => {
    await global.Olm.init();

    const config = JSON.parse(readFileSync("config.json", {encoding: "utf8"}));
    config.request = request;

    const runner = new TestRunner(config);

    try {
        await test(runner);
        runner.log("✅ Test succeeded");
    } catch (e) {
        runner.log("Test failed:", e);
    } finally {
        await runner.doCleanup();
    }
})();
