/*
Copyright 2021 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import anotherjson from "another-json";

let _olmUtil: Olm.Utility;
function olmUtil(): Olm.Utility {
    if (!_olmUtil) {
        _olmUtil = new global.Olm.Utility();
    }
    return _olmUtil;
}

export type JsonObj = Record<string, any>;

export function strippedJsonForSigning(obj: JsonObj): string {
    const strippedObj = Object.assign({}, obj);
    delete strippedObj.unsigned;
    delete strippedObj.signatures;
    return anotherjson.stringify(strippedObj);
}

export function verifySignedJson(
    obj: JsonObj, key: string, userId: string, unprefixedKeyId: string,
) {
    const json = strippedJsonForSigning(obj);

    olmUtil().ed25519_verify(key, json, obj.signatures[userId][`ed25519:${unprefixedKeyId}`]);
}

let counter = 0;
export function makeTxnId(): string {
    return (++counter).toString(); // FIXME: also add a time
}
