// until http-api gets converted to TypeScript

//import { EventEmitter } from "events";

declare module "matrix-js-sdk/lib/http-api" {
    export const PREFIX_R0: string;
    export class MatrixHttpApi {
        constructor(emitter: any, opts: Record<string, any>);
        request(
            callback: any,
            method: string,
            path: string,
            query: Record<string, string> | undefined,
            body: Record<string, any> | undefined,
        ) : Promise<Record<string, any>>;
        authedRequest(
            callback: any,
            method: string,
            path: string,
            query: Record<string, string> | undefined,
            body: Record<string, any> | undefined,
        ) : Promise<Record<string, any>>;
    }
}
