/*
Copyright 2021 The Matrix.org Foundation C.I.C.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import * as readline from "readline";
import { stdin as input, stdout as output } from 'process';

import { MatrixHttpApi, PREFIX_R0 } from "matrix-js-sdk/lib/http-api";
import { EventEmitter } from "events";

import { JsonObj } from "./util"

const rl = readline.createInterface({ input, output });

export interface ITestRunnerOpts {
    baseUrl: string,
    userId: string,
    password: string,
    // function to call for HTTP requests
    request: (opts: any, callback: any) => void,
}

export class TestRunner {
    private devices: TestUser[] = [];
    private readonly http: MatrixHttpApi;
    private cleanupTasks: (() => Promise<void> | void)[] = [];
    constructor(private readonly opts: ITestRunnerOpts) {
        this.http = new MatrixHttpApi(
            // we're just using this for logging in, so we don't care about
            // events
            new EventEmitter(),
            {
                baseUrl: opts.baseUrl,
                request: opts.request,
                prefix: PREFIX_R0,
                onlyData: true,
            },
        )
    }
    async logIn(): Promise<TestUser> {
        const res = await this.http.request(undefined, "POST", "/login", undefined, {
            "type": "m.login.password",
            "identifier": {
                "type": "m.id.user",
                "user": this.opts.userId,
            },
            "password": this.opts.password,
        });
        const user = new TestUser(res.user_id, res.device_id, {
            baseUrl: this.opts.baseUrl,
            request: this.opts.request,
            prefix: PREFIX_R0,
            onlyData: true,
            accessToken: res.access_token,
            useAuthorizationHeader: true,
        });
        this.devices.push(user);
        return user;
    }

    cleanup(task: () => Promise<void> | void): void {
        this.cleanupTasks.push(task);
    }
    async doCleanup(): Promise<void> {
        try {
            for (const task of this.cleanupTasks) {
                await task();
            }
        } finally {
            await Promise.all(this.devices.map((device) => device.logOut()));
        }
    }

    async prompt(prompt: string): Promise<string> {
        return new Promise((resolve) => {
            rl.question(prompt + " ", (res) => {
                resolve(res);
            });
        });
    }
    async promptYN(prompt: string): Promise<boolean> {
        const response = await this.prompt(prompt + " [y/n]");
        if (response.length > 0) {
            switch (response[0].toLowerCase()) {
                case "y":
                    return true;
                case "n":
                    return false;
            }
        }
        console.log('Please enter "y" or "n"');
        return this.promptYN(prompt);
    }
    async waitForUser(prompt: string): Promise<void> {
        this.log(prompt);
        await this.prompt("Hit enter when ready.");
    }

    log(...args: any[]): void {
        console.log(...args);
    }
    info(...args: any[]): void {
        console.info(...args);
    }
    warn(...args: any[]): void {
        console.warn(...args);
    }
    debug(...args: any[]): void {
        console.debug(...args);
    }
}

export type RoomState = Record<string, Record<string, JsonObj>>;

export type RoomInfo = {
    state: RoomState,
    events: JsonObj[],
}

export interface ICreateRoomOpts {
    encrypted?: boolean,
    invite?: string[],
    // FIXME: ...
}

export class TestUser extends EventEmitter {
    public readonly http: MatrixHttpApi;
    public rooms: Record<string, RoomInfo> = {};
    private syncToken?: string;
    constructor(
        public readonly userId: string,
        public readonly deviceId: string,
        opts: Record<string, any>,
    ) {
        super();
        this.http = new MatrixHttpApi(this, opts);
    }
    async logOut(): Promise<void> {
        try {
            await this.http.authedRequest(
                undefined, "POST", "/logout", undefined, {},
            );
        } catch (e) {}
    }
    async createRoom(opts: ICreateRoomOpts): Promise<string> {
        const roomParams: JsonObj = {};
        if (opts.invite) {
            roomParams.invite = opts.invite;
        }
        if (opts.encrypted) {
            roomParams.initialState = [{
                type: "m.room.encryption",
                state_key: "",
                content: {
                    algorithm: "m.megolm.v1-aes-sha2",
                }
            }]
        }
        const res = await this.http.authedRequest(
            undefined, "POST", "/createRoom", undefined, roomParams,
        );
        return res.room_id;
    }
    async startSync(): Promise<void> {
        for (;;) {
            const queryParams: Record<string, string> = {timeout: "30000"};
            if (this.syncToken) {
                queryParams.since = this.syncToken;
            }
            let res;
            try {
                res = await this.http.authedRequest(
                    undefined, "GET", "/sync", queryParams, undefined,
                );
            } catch (e) {
                this.emit("syncStopped");
                return;
            }
            if (res.next_batch) {
                this.syncToken = res.next_batch;
            }
            if (res.to_device?.events) {
                res.to_device.events.forEach((event: JsonObj) => this.emit("toDevice", event));
            }
            if (res.rooms) {
                if (res.rooms.invite) {
                    for (const [roomId, room] of Object.entries(res.rooms.invite) as [string, JsonObj][]) {
                        this.emit("invite", {roomId, state: room.invite_state});
                    }
                }
                if (res.rooms.join) {
                    for (const [roomId, room] of Object.entries(res.rooms.join) as [string, JsonObj][]) {
                        if (room.timeline && room.timeline.limited) {
                            this.emit("limited", {room: roomId, prevBatch: room.prev_batch});
                        }
                        if (room.state && room.state.events) {
                            for (const event of room.state.events) {
                                event.room_id = roomId;
                                this.emit("roomEvent", event, true);
                            }
                        }
                        if (room.timeline && room.timeline.events) {
                            for (const event of room.timeline.events) {
                                event.room_id = roomId;
                                this.emit("roomEvent", event);
                            }
                        }
                    }
                }
                if (res.rooms.leave) {
                    for (const [roomId, room] of Object.entries(res.rooms.leave) as [string, JsonObj][]) {
                        if (room.timeline && room.timeline.limited) {
                            this.emit("limited", {room: roomId, prevBatch: room.prev_batch});
                        }
                        if (room.state && room.state.events) {
                            for (const event of room.state.events) {
                                event.room_id = roomId;
                                this.emit("roomEvent", event, true);
                            }
                        }
                        if (room.timeline && room.timeline.events) {
                            for (const event of room.timeline.events) {
                                event.room_id = roomId;
                                this.emit("roomEvent", event);
                            }
                        }
                        this.emit("left", roomId);
                    }
                }
            }
        }
    }

    async waitForEvent(test: (event: JsonObj) => boolean): Promise<void> {
        await new Promise((resolve, reject) => {
            const listener = (event: JsonObj) => {
                if (test(event)) {
                    this.off("roomEvent", listener);
                    resolve(undefined);
                }
            };
            this.on("roomEvent", listener);
            // FIXME: add a timeout?
        });
    }
}
