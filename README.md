Matrix Client Test Bot
======================

This is a bot for testing clients.  Since it is a standalone bot, it can be
used to test things that may be hard to reproduce in a normal client.

To use this, you will need to create a `config.json` file of the form:

```json
{
    "baseUrl": "https://matrix.example.org",
    "userId": "@testbot_user:example.org",
    "password": "thisIsASecret"
}
```

You can install the dependencies by running `yarn install` and build from
source by running `yarn build`.

Then you can run `yarn test <testname>`. (`testname` is currently ignored since
there is only one test available), and follow the instructions and prompts.

## Tests

### fallback

This tests that a client is able to upload and maintain Olm fallback keys.

You first log in using the client.  This should upload one-time keys and a
fallback key to the server.  You then tell the bot your user ID and device ID
so that it knows which device to test.  Then you close the client so that it no
longer syncs with the server, and won't upload new one-time keys or fallback
keys.  The bot then claims all of your client's keys, until it obtains the
fallback key.

The bot also invites your user to a room, in which it sends two messages, each
encrypted with a separate megolm session.  The two megolm sessions are shared
over two olm sessions, both created using the fallback key.  When you restart
the client and join the room, you should be able to decrypt both messages in
the room.

Since the fallback key has been used, the client should then upload a new
fallback key and, after some time, forget the old fallback key.  The bot will
wait for 6 minutes and send another message to the room, using a new megolm
session, sent using another olm session created using the old fallback key
again.  Since the client has forgotten about the old fallback key, it should be
unable to decrypt this message.
